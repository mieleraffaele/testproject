<li class="{{ Request::is('testNames*') ? 'active' : '' }}">
    <a href="{{ route('testNames.index') }}"><i class="fa fa-edit"></i><span>Test Names</span></a>
</li>


<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-user"></i><span>Users</span></a>
</li>
