@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Test Name
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($testName, ['route' => ['testNames.update', $testName->id], 'method' => 'patch']) !!}

                        @include('test_names.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection