<div class="table-responsive">
    <table class="table" id="testNames-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Email</th>
        <th>Writer Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($testNames as $testName)
            <tr>
                <td>{{ $testName->title }}</td>
            <td>{{ $testName->email }}</td>
            <td>{{ $testName->writer_id }}</td>
                <td>
                    {!! Form::open(['route' => ['testNames.destroy', $testName->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('testNames.show', [$testName->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('testNames.edit', [$testName->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
