<?php namespace Tests\Repositories;

use App\Models\TestName;
use App\Repositories\TestNameRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TestNameRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TestNameRepository
     */
    protected $testNameRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->testNameRepo = \App::make(TestNameRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_test_name()
    {
        $testName = factory(TestName::class)->make()->toArray();

        $createdTestName = $this->testNameRepo->create($testName);

        $createdTestName = $createdTestName->toArray();
        $this->assertArrayHasKey('id', $createdTestName);
        $this->assertNotNull($createdTestName['id'], 'Created TestName must have id specified');
        $this->assertNotNull(TestName::find($createdTestName['id']), 'TestName with given id must be in DB');
        $this->assertModelData($testName, $createdTestName);
    }

    /**
     * @test read
     */
    public function test_read_test_name()
    {
        $testName = factory(TestName::class)->create();

        $dbTestName = $this->testNameRepo->find($testName->id);

        $dbTestName = $dbTestName->toArray();
        $this->assertModelData($testName->toArray(), $dbTestName);
    }

    /**
     * @test update
     */
    public function test_update_test_name()
    {
        $testName = factory(TestName::class)->create();
        $fakeTestName = factory(TestName::class)->make()->toArray();

        $updatedTestName = $this->testNameRepo->update($fakeTestName, $testName->id);

        $this->assertModelData($fakeTestName, $updatedTestName->toArray());
        $dbTestName = $this->testNameRepo->find($testName->id);
        $this->assertModelData($fakeTestName, $dbTestName->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_test_name()
    {
        $testName = factory(TestName::class)->create();

        $resp = $this->testNameRepo->delete($testName->id);

        $this->assertTrue($resp);
        $this->assertNull(TestName::find($testName->id), 'TestName should not exist in DB');
    }
}
