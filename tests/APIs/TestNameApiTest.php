<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TestName;

class TestNameApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_test_name()
    {
        $testName = factory(TestName::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/test_names', $testName
        );

        $this->assertApiResponse($testName);
    }

    /**
     * @test
     */
    public function test_read_test_name()
    {
        $testName = factory(TestName::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/test_names/'.$testName->id
        );

        $this->assertApiResponse($testName->toArray());
    }

    /**
     * @test
     */
    public function test_update_test_name()
    {
        $testName = factory(TestName::class)->create();
        $editedTestName = factory(TestName::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/test_names/'.$testName->id,
            $editedTestName
        );

        $this->assertApiResponse($editedTestName);
    }

    /**
     * @test
     */
    public function test_delete_test_name()
    {
        $testName = factory(TestName::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/test_names/'.$testName->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/test_names/'.$testName->id
        );

        $this->response->assertStatus(404);
    }
}
