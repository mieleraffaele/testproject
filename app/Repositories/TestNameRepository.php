<?php

namespace App\Repositories;

use App\Models\TestName;
use App\Repositories\BaseRepository;

/**
 * Class TestNameRepository
 * @package App\Repositories
 * @version November 4, 2020, 12:09 pm UTC
*/

class TestNameRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TestName::class;
    }
}
