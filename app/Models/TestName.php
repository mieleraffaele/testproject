<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class TestName
 * @package App\Models
 * @version November 4, 2020, 12:09 pm UTC
 *
 * @property string $title
 * @property string $body
 * @property string $email
 * @property integer $writer_id
 */
class TestName extends Model
{
    use SoftDeletes;

    public $table = 'test_names';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'body',
        'email',
        'writer_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'body' => 'string',
        'email' => 'string',
        'writer_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
