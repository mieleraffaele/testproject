<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTestNameRequest;
use App\Http\Requests\UpdateTestNameRequest;
use App\Repositories\TestNameRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TestNameController extends AppBaseController
{
    /** @var  TestNameRepository */
    private $testNameRepository;

    public function __construct(TestNameRepository $testNameRepo)
    {
        $this->testNameRepository = $testNameRepo;
    }

    /**
     * Display a listing of the TestName.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $testNames = $this->testNameRepository->all();

        return view('test_names.index')
            ->with('testNames', $testNames);
    }

    /**
     * Show the form for creating a new TestName.
     *
     * @return Response
     */
    public function create()
    {
        return view('test_names.create');
    }

    /**
     * Store a newly created TestName in storage.
     *
     * @param CreateTestNameRequest $request
     *
     * @return Response
     */
    public function store(CreateTestNameRequest $request)
    {
        $input = $request->all();

        $testName = $this->testNameRepository->create($input);

        Flash::success('Test Name saved successfully.');

        return redirect(route('testNames.index'));
    }

    /**
     * Display the specified TestName.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            Flash::error('Test Name not found');

            return redirect(route('testNames.index'));
        }

        return view('test_names.show')->with('testName', $testName);
    }

    /**
     * Show the form for editing the specified TestName.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            Flash::error('Test Name not found');

            return redirect(route('testNames.index'));
        }

        return view('test_names.edit')->with('testName', $testName);
    }

    /**
     * Update the specified TestName in storage.
     *
     * @param int $id
     * @param UpdateTestNameRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTestNameRequest $request)
    {
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            Flash::error('Test Name not found');

            return redirect(route('testNames.index'));
        }

        $testName = $this->testNameRepository->update($request->all(), $id);

        Flash::success('Test Name updated successfully.');

        return redirect(route('testNames.index'));
    }

    /**
     * Remove the specified TestName from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            Flash::error('Test Name not found');

            return redirect(route('testNames.index'));
        }

        $this->testNameRepository->delete($id);

        Flash::success('Test Name deleted successfully.');

        return redirect(route('testNames.index'));
    }
}
