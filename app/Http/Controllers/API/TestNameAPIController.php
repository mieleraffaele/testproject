<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTestNameAPIRequest;
use App\Http\Requests\API\UpdateTestNameAPIRequest;
use App\Models\TestName;
use App\Repositories\TestNameRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TestNameController
 * @package App\Http\Controllers\API
 */

class TestNameAPIController extends AppBaseController
{
    /** @var  TestNameRepository */
    private $testNameRepository;

    public function __construct(TestNameRepository $testNameRepo)
    {
        $this->testNameRepository = $testNameRepo;
    }

    /**
     * Display a listing of the TestName.
     * GET|HEAD /testNames
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $testNames = $this->testNameRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($testNames->toArray(), 'Test Names retrieved successfully');
    }

    /**
     * Store a newly created TestName in storage.
     * POST /testNames
     *
     * @param CreateTestNameAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTestNameAPIRequest $request)
    {
        $input = $request->all();

        $testName = $this->testNameRepository->create($input);

        return $this->sendResponse($testName->toArray(), 'Test Name saved successfully');
    }

    /**
     * Display the specified TestName.
     * GET|HEAD /testNames/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TestName $testName */
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            return $this->sendError('Test Name not found');
        }

        return $this->sendResponse($testName->toArray(), 'Test Name retrieved successfully');
    }

    /**
     * Update the specified TestName in storage.
     * PUT/PATCH /testNames/{id}
     *
     * @param int $id
     * @param UpdateTestNameAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTestNameAPIRequest $request)
    {
        $input = $request->all();

        /** @var TestName $testName */
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            return $this->sendError('Test Name not found');
        }

        $testName = $this->testNameRepository->update($input, $id);

        return $this->sendResponse($testName->toArray(), 'TestName updated successfully');
    }

    /**
     * Remove the specified TestName from storage.
     * DELETE /testNames/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TestName $testName */
        $testName = $this->testNameRepository->find($id);

        if (empty($testName)) {
            return $this->sendError('Test Name not found');
        }

        $testName->delete();

        return $this->sendSuccess('Test Name deleted successfully');
    }
}
